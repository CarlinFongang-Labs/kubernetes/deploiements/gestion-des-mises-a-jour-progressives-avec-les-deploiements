# Gestion des mises à jour progressives avec les déploiements : Rolling Update et Rollback

------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


# Gestion des mises à jour continues avec les déploiements

Dans cette leçon, nous allons parler de la gestion des mises à jour continues avec les déploiements. Voici un aperçu rapide des sujets que nous allons aborder :

1. Qu'est-ce qu'une mise à jour continue ?
2. Qu'est-ce qu'un rollback ?
3. Une démonstration pratique de la mise à jour continue et des rollbacks avec les déploiements Kubernetes.

# Qu'est-ce qu'une mise à jour continue ou Rolling Update ?

Une mise à jour continue vous permet de faire des modifications aux pods d'un déploiement à un rythme contrôlé. Nous remplaçons progressivement les anciens pods par de nouveaux pods, plutôt que de remplacer tous les pods en une seule fois. Cela permet de mettre à jour les pods sans interruption de service.

Par exemple, si nous voulons déployer une nouvelle version de notre logiciel, nous pouvons le faire en utilisant une mise à jour continue sans avoir besoin de mettre notre déploiement hors ligne et de provoquer une interruption de service. Nous remplaçons progressivement les anciens pods par des nouveaux pods exécutant la nouvelle version.

# Qu'est-ce qu'un rollback ?

Les rollbacks vont de pair avec les mises à jour continues. Les mises à jour continues permettent de faire des modifications aux déploiements. Les rollbacks sont ce que nous faisons lorsque nous réalisons que nous ne voulons pas de ces modifications parce qu'elles ont causé des problèmes. Un rollback nous permet de revenir à une version précédente fonctionnelle du déploiement.

# Démonstration pratique

Voyons à quoi ressemblent les mises à jour continues et les rollbacks dans notre cluster Kubernetes.

1. **Connexion au nœud de contrôle**

   Connectez-vous à votre nœud de contrôle Kubernetes.

```sh
ssh -i id_rsa cloud@PUBLIC_IP_ADDRESS
```

2. **Édition du déploiement pour une mise à jour continue**

   Nous allons utiliser la méthode `kubectl edit` pour éditer notre déploiement.

```sh
kubectl edit deployment my-deployment
```

   Recherchez le modèle de pod (`template`) dans le descripteur YAML du déploiement. Sous la spécification du déploiement, trouvez le modèle de pod et la définition du conteneur. Changez l'image de `nginx:1.19.1` à `nginx:1.19.2`.

   Enregistrez et quittez le fichier.

   Cela déclenche automatiquement une mise à jour continue. Utilisez la commande suivante pour voir le statut du déploiement :

```sh
kubectl rollout status deployment.v1.apps/my-deployment
```

   Vous verrez le statut de la mise à jour continue. Si la mise à jour est rapide, elle peut déjà être terminée avant que vous ne vérifiiez le statut.

3. **Utilisation de `kubectl set image` pour une mise à jour continue**

   Vous pouvez également utiliser la commande `kubectl set image` pour mettre à jour l'image d'un conteneur :

```sh
kubectl set image deployment/my-deployment nginx=nginx:broken --record
```

   Cette commande change l'image du conteneur en une version défectueuse (`nginx:broken`) pour démontrer un échec.

   Vérifiez le statut du déploiement :

```sh
kubectl rollout status deployment.v1.apps/my-deployment
```

   Vous verrez que la mise à jour est en attente car elle ne peut pas trouver l'image. Annulez la commande avec `Ctrl+C`.

   Utilisez `kubectl get pods` pour voir les pods :

```sh
kubectl get pods
```

   Vous verrez les anciens pods toujours en cours d'exécution et un nouveau pod avec une erreur de récupération d'image (`ImagePullError`).

4. **Historique des déploiements et rollback**

   Utilisez la commande `kubectl rollout history` pour voir l'historique des déploiements :

```sh
kubectl rollout history deployment.v1.apps/my-deployment
```

   Cela montre les différentes révisions du déploiement. La révision 2 est celle où tout fonctionnait, et la révision 3 est la version défectueuse.

   Pour revenir à une version précédente, utilisez la commande `kubectl rollout undo` :

```sh
kubectl rollout undo deployment.v1.apps/my-deployment
```

   Par défaut, cela revient à la version précédente (révision 2). Vous pouvez spécifier une révision avec le tag `--to-revision`.

   Vérifiez à nouveau les pods :

```sh
kubectl get pods
```

   Vous verrez que le pod défectueux a été supprimé.

   Vérifiez le statut du déploiement :

```sh
kubectl rollout status deployment.v1.apps/my-deployment
```

   Vous verrez que le déploiement a été restauré avec succès.

# Conclusion

Pour récapituler, nous avons répondu à la question "Qu'est-ce qu'une mise à jour continue ?", nous avons expliqué ce qu'est un rollback, et nous avons réalisé une démonstration pratique pour montrer comment effectuer des mises à jour continues et des rollbacks dans Kubernetes. C'est tout pour cette leçon. À la prochaine !


# Reférences



https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#updating-a-deployment

https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#rolling-back-a-deployment